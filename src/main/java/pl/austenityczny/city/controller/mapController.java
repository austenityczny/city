package pl.austenityczny.city.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.austenityczny.city.weather.dto.WeatherDTO;
import pl.austenityczny.city.weather.service.WeatherService;
import pl.austenityczny.city.wiki.dto.WikiDTO;
import pl.austenityczny.city.wiki.service.WikiService;

@Slf4j
@Controller
@RequiredArgsConstructor
public class mapController {

    private final WeatherService weatherService;
    private final WikiService wikiService;

    @GetMapping("")
    public String getMap(@RequestParam(defaultValue = "Warszawa") String city, Model model){

        model.addAttribute("city", city);

        WikiDTO wikiDTO = wikiService.getWikiDTO(city);
        model.addAttribute("wikiDTO", wikiDTO);

        WeatherDTO weatherDTO = weatherService.getWeather(wikiDTO.getLat(), wikiDTO.getLon());
        model.addAttribute("weather", weatherDTO);

        log.info(wikiDTO.toString() + weatherDTO.toString());
        return "map";
    }

}
