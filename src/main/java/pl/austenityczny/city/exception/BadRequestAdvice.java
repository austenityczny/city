package pl.austenityczny.city.exception;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BadRequestAdvice {

    @ExceptionHandler(BadRequestException.class)
    public String badRequestHandler(BadRequestException ex){
        return "/error";
    }
}
