package pl.austenityczny.city.weather.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.austenityczny.city.weather.client.dto.OpenWeatherDTO;
import pl.austenityczny.city.weather.dto.WeatherDTO;

@Slf4j
@Component
public class WeatherClient {

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/onecall?";
    private static final String API_KEY = "appid=b6f5f46da2df2ccda6dd5ffef72204c7";

    private RestTemplate restTempalate = new RestTemplate();

    public WeatherDTO getWeather(double lat, double lon){
        OpenWeatherDTO response = restTempalate.getForObject(WEATHER_URL + "lat={lat}&lon={lon}&exclude=minutely&units=metric&lang=pl&" + API_KEY,
                OpenWeatherDTO.class, lat, lon);
        return WeatherDTO.builder()
                .temperature(response.getCurrent().getTemp())
                .pressure(response.getCurrent().getPressure())
                .humidity(response.getCurrent().getHumidity())
                .windSpeed(response.getCurrent().getWind_speed())
                .build();
    }
}
