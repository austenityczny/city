package pl.austenityczny.city.weather.client.dto;

import lombok.Getter;

@Getter
public class OpenWeatherCurrentDTO {

    private float temp;
    private int pressure;
    private int humidity;
    private float wind_speed;

}
