package pl.austenityczny.city.weather.client.dto;

import lombok.Getter;

@Getter
public class OpenWeatherDTO {

    private OpenWeatherCurrentDTO current;
}
