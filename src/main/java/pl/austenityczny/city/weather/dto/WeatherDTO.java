package pl.austenityczny.city.weather.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@Builder
public class WeatherDTO {

    private float temperature;
    private int pressure;
    private int humidity;
    private float windSpeed;
}
