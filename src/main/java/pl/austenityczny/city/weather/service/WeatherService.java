package pl.austenityczny.city.weather.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.austenityczny.city.weather.client.WeatherClient;
import pl.austenityczny.city.weather.dto.WeatherDTO;

@Slf4j
@Component
@RequiredArgsConstructor
public class WeatherService {

    private final WeatherClient weatherClient;

    public WeatherDTO getWeather(Double lat, Double lon){
        return weatherClient.getWeather(lat, lon);
    }
}
