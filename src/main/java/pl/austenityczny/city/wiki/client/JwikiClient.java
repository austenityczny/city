package pl.austenityczny.city.wiki.client;

import Jwiki.Jwiki;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import pl.austenityczny.city.exception.BadRequestException;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JwikiClient {

    private Jwiki jwiki;

    public JwikiClient(String city) {
        this.jwiki = new Jwiki(city);
    }

    public String getDisplayTitle(){
        return jwiki.getDisplayTitle();
    }

    public String getText(){
        return jwiki.getExtractText();
    }

    public String getWikiURL(){ return jwiki.getWikiURL(); }

    public Double getLat(){
//        if (jwiki.getLat() == null) {
//            throw new BadRequest(" ")
//        }
        return Optional.ofNullable(jwiki.getLat()).orElseThrow(() -> new BadRequestException("Nie znaleziono koordynatów. Prawdopodobnie nie wpisałeś nazwy miasta.")); }

    public Double getLon(){ return Optional.ofNullable(jwiki.getLon()).orElseThrow(() -> new BadRequestException("Nie znaleziono koordynatów. Prawdopodobnie nie wpisałeś nazwy miasta.")); }

    public String getImageURL(){ return Optional.ofNullable(jwiki.getImageURL()).orElseThrow(() -> new BadRequestException("Nie znaleziono zdjęcia.")); }

}
