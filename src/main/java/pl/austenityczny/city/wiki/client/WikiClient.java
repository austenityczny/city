package pl.austenityczny.city.wiki.client;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.austenityczny.city.wiki.dto.WikiDTO;

import java.util.List;

@Data
@RequiredArgsConstructor
@Service
public class WikiClient {

    private WikiDTO wikiDTO;
    private Double lon;
    private Double lat;
    


    public WikiDTO getWikiDTO(String city) {
        JwikiClient jwiki = new JwikiClient(city);
        return WikiDTO.builder()
                .city(jwiki.getDisplayTitle())
                .text(jwiki.getText())
                .imageURL(jwiki.getImageURL())
                .wikiURL(jwiki.getWikiURL())
                .lon(jwiki.getLon())
                .lat(jwiki.getLat())
                .build();
    }


}
