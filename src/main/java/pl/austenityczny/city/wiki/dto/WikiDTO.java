package pl.austenityczny.city.wiki.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class WikiDTO{

    private String city;
    private String text;
    private String imageURL;
    private String wikiURL;
    private double lon;
    private double lat;

}
