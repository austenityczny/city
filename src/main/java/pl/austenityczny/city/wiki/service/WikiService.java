package pl.austenityczny.city.wiki.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.austenityczny.city.wiki.client.WikiClient;
import pl.austenityczny.city.wiki.dto.WikiDTO;

@Component
@RequiredArgsConstructor
public class WikiService {

    private final WikiClient wikiClient;

    public WikiDTO getWikiDTO(String city){
        return wikiClient.getWikiDTO(city);
    }


}
